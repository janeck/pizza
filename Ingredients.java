package co.uk.zadania_lukasz_006;

import java.util.List;

public interface Ingredients {

    List<String> getIngredients();
}
