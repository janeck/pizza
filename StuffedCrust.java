package co.uk.zadania_lukasz_006;

public class StuffedCrust implements  PizzaDough{
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta Stuffed Crust");

    }

    @Override
    public String toString() {
        return "Stuffed Crust";
    }
}
