package co.uk.zadania_lukasz_006;

public class GlutenFree implements  PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta Gluten Free");
    }

    @Override
    public String toString() {
        return "Gluten Free";
    }
}
