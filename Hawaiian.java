package co.uk.zadania_lukasz_006;

import java.util.List;
import java.util.StringJoiner;

public class Hawaiian implements Pizza, Ingredients  {
    private List<String> ingredients;
    private PizzaDough pizzaDough;


    public Hawaiian() {
    }

    public Hawaiian(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    public List<String> getIngredients() {
        return ingredients;
    }



    @Override
    public String toString() {
        return new StringJoiner(", ", Hawaiian.class.getSimpleName() + "[", "]")
                .add("ingredients=" + ingredients)
                .add("pizzaDough=" + pizzaDough)
                .toString();
    }

    @Override
    public void preparePizza() {
        System.out.println("Kroki potrzebne do wykonania pizzy Hawaiian: " );
        System.out.print("Krok 1: "); pizzaDough.preparePizzaDough();
        System.out.println("Krok 2: Posmaruj ciasta baza, dodaj ziola.");
        System.out.println("Krok 3: Dodaj skladniki z listy skkladnikow");
        System.out.println("Krok 4: Do pieca na 20 min.");

    }
}
