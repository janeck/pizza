package co.uk.zadania_lukasz_006;

public class FeelGoodFlatbread implements PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta Feel Good Flat bread");

    }

    @Override
    public String toString() {
        return "Feel Good Flat bread";
    }
}
