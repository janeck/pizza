package co.uk.zadania_lukasz_006;

public class ChessyBites implements PizzaDough {


    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta Chessy Bites");
    }

    @Override
    public String toString() {
        return "Chessy Bites";
    }
}
