package co.uk.zadania_lukasz_006;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        ArrayList<Pizza> pizzas = new ArrayList<>();

 Margherita margherita = new Margherita(List.of("Ser","Pomidory"), new AllAmericanThinii());
 Veggie veggie = new Veggie(List.of("ser","Kapusta","Papryka","Kukurydza"), new Pan());
 Hawaiian hawaiian  = new Hawaiian(List.of("boczek","ser","szynka","Kukurydza"), new ChessyBites());


        pizzas.add(margherita);
        pizzas.add(veggie);
        pizzas.add(hawaiian);

        for (int i = 0; i < pizzas.size(); i++){
        pizzas.get(i).preparePizza();
        System.out.println();

        }


        System.out.println(margherita);
        System.out.println(veggie);
        System.out.println(hawaiian);

    }
}